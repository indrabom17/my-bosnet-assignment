//EmployeeLIst
let EmployeeLIst = [
      {
        "no": 1,
        "ID": "BDI-001",
        "name": "John Doe",
        "birthday": "1990-03-15",
        "age": "33 years 4 months 21 days"
      },
      {
        "no": 2,
        "ID": "BDI-002",
        "name": "Jane Smith",
        "birthday": "1985-09-22",
        "age": "37 years 10 months 14 days"
      },
      {
        "no": 3,
        "ID": "BDI-003",
        "name": "Bob Johnson",
        "birthday": "2001-12-05",
        "age": "21 years 8 months 0 days"
      },
      {
        "no": 4,
        "ID": "BDI-004",
        "name": "Alice Brown",
        "birthday": "1995-06-30",
        "age": "28 years 1 month 6 days"
      },
      {
        "no": 5,
        "ID": "BDI-005",
        "name": "Michael Lee",
        "birthday": "1989-11-10",
        "age": "33 years 8 months 26 days"
      },
      {
        "no": 6,
        "ID": "BDI-006",
        "name": "Sophia Nguyen",
        "birthday": "1998-04-02",
        "age": "25 years 4 months 3 days"
      },
      {
        "no": 7,
        "ID": "BDI-007",
        "name": "James Kim",
        "birthday": "1994-12-17",
        "age": "28 years 7 months 19 days"
      },
      {
        "no": 8,
        "ID": "BDI-008",
        "name": "Emily Wilson",
        "birthday": "1986-07-25",
        "age": "37 years 0 months 11 days"
      },
      {
        "no": 9,
        "ID": "BDI-009",
        "name": "William Martin",
        "birthday": "2003-02-08",
        "age": "20 years 6 months 28 days"
      },
      {
        "no": 10,
        "ID": "BDI-010",
        "name": "Olivia Turner",
        "birthday": "1992-09-12",
        "age": "30 years 10 months 24 days"
      },
      {
        "no": 11,
        "ID": "BDI-011",
        "name": "Daniel Anderson",
        "birthday": "1997-05-28",
        "age": "26 years 2 months 8 days"
      },
      {
        "no": 12,
        "ID": "BDI-012",
        "name": "Ava Martinez",
        "birthday": "1996-08-04",
        "age": "27 years 11 months 1 day"
      },
      {
        "no": 13,
        "ID": "BDI-013",
        "name": "Joseph Hernandez",
        "birthday": "1993-11-01",
        "age": "29 years 9 months 4 days"
      },
      {
        "no": 14,
        "ID": "BDI-014",
        "name": "Samantha Lopez",
        "birthday": "2002-01-19",
        "age": "21 years 6 months 17 days"
      },
      {
        "no": 15,
        "ID": "BDI-015",
        "name": "David Kim",
        "birthday": "1987-10-03",
        "age": "35 years 10 months 2 days"
      },
      {
        "no": 16,
        "ID": "BDI-016",
        "name": "Elizabeth Wang",
        "birthday": "1998-03-07",
        "age": "25 years 5 months 29 days"
      },
      {
        "no": 17,
        "ID": "BDI-017",
        "name": "Michael Garcia",
        "birthday": "1991-12-20",
        "age": "31 years 7 months 6 days"
      },
      {
        "no": 18,
        "ID": "BDI-018",
        "name": "Isabella Brown",
        "birthday": "2000-06-12",
        "age": "23 years 1 month 14 days"
      },
      {
        "no": 19,
        "ID": "BDI-019",
        "name": "Ethan Nguyen",
        "birthday": "1996-04-30",
        "age": "27 years 3 months 27 days"
      },
      {
        "no": 20,
        "ID": "BDI-020",
        "name": "Sophia Rodriguez",
        "birthday": "1997-09-05",
        "age": "25 years 10 months 0 days"
      },
      {
        "no": 21,
        "ID": "BDI-021",
        "name": "Alexander Martinez",
        "birthday": "1993-08-14",
        "age": "28 years 11 months 22 days"
      },
      {
        "no": 22,
        "ID": "BDI-022",
        "name": "Mia Wilson",
        "birthday": "1990-02-28",
        "age": "31 years 5 months 27 days"
      },
      {
        "no": 23,
        "ID": "BDI-023",
        "name": "Daniel Nguyen",
        "birthday": "2002-07-01",
        "age": "21 years 1 month 24 days"
      },
      {
        "no": 24,
        "ID": "BDI-024",
        "name": "Emma Taylor",
        "birthday": "1998-10-29",
        "age": "22 years 8 months 7 days"
      },
      {
        "no": 25,
        "ID": "BDI-025",
        "name": "Jackson Brown",
        "birthday": "1986-05-17",
        "age": "35 years 2 months 19 days"
      },
      {
        "no": 26,
        "ID": "BDI-026",
        "name": "Ava Rodriguez",
        "birthday": "1994-06-25",
        "age": "27 years 1 month 11 days"
      },
      {
        "no": 27,
        "ID": "BDI-027",
        "name": "Liam Wilson",
        "birthday": "1987-03-12",
        "age": "34 years 4 months 24 days"
      },
      {
        "no": 28,
        "ID": "BDI-028",
        "name": "Olivia Kim",
        "birthday": "1999-01-07",
        "age": "24 years 7 months 29 days"
      },
      {
        "no": 29,
        "ID": "BDI-029",
        "name": "Noah Lee",
        "birthday": "2000-04-14",
        "age": "21 years 3 months 22 days"
      },
      {
        "no": 30,
        "ID": "BDI-030",
        "name": "Sophia Johnson",
        "birthday": "1991-11-19",
        "age": "31 years 8 months 7 days"
      }
    ]
  
class EmployeeData{
    EmployeeList = [];
    EmployeeDataLengt = this.EmployeeList.length;
    leadingIndex = 0;
    tailingIndex =0;

    constructor(employeeData){
        this.EmployeeList = employeeData;
    }

    defaultTable_View(){
        /**
         * Return Default Table View / populate Data
         */
        this.leadingIndex = 0;
        this.tailingIndex = 5;
        let paged_data = [];
        this.EmployeeList.length >= 5 ? this.tailingIndex = 5 :
            this.tailingIndex = this.EmployeeList.length;
       for (let index = 0; index < this.tailingIndex; index++) {
            paged_data.push(this.EmployeeList[index]);
       }
       return paged_data
    }

    purgeTable(){
        $('#t_content').empty();
    }

    populateTable(table_data){
        table_data.forEach(ele => {
            $('#t_content').append(
                `<tr>
                    <td>${ele.no}</td>
                    <td>${ele.ID}</td>
                    <td>${ele.name}</td>
                    <td>${ele.birthday}</td>
                    <td>${ele.age}</td>
                </tr>`
                )
        });
    }

    btnFirst_Click(){
        /**
         * Return Set of First Data
         */
        let paged_data = [];
        let tailIndex = 0
        this.leadingIndex = 0;
        const limit = this.EmployeeList.length > 5 ? tailIndex = 5 : 
                        tailIndex = this.EmployeeList.length;
        this.tailingIndex = tailIndex;
        for (let index = 0; index < tailIndex; index++) { 
            paged_data.push(this.EmployeeList[index])
        }

        return paged_data;
        
        console.log(this.EmployeeList[0].name);
    }

    btnPreviousPage_Click(){
        let paged_data = [];
        console.log(this.leadingIndex);
        if(this.leadingIndex <= 0) return false;
        this.leadingIndex = this.leadingIndex - 5;
        this.tailingIndex = this.tailingIndex - 5;
        this.leadingIndex <= 0 ? this.leadingIndex = 0:'';
        this.EmployeeList.length >= 5 ? this.tailingIndex = this.leadingIndex + 5:
            this.tailingIndex = this.EmployeeList.length 
        for (let index = this.leadingIndex; index < this.tailingIndex; index++) {
            paged_data.push(this.EmployeeList[index]);
        }
        return paged_data;
    }

    btnPrevious_Click(){
        /**
         * Return Prev list Data
         */
        let paged_data = [];
        if(this.leadingIndex == 0) return false;
        this.leadingIndex = this.leadingIndex - 1;
        this.tailingIndex = this.tailingIndex - 1;
        for (let index = this.leadingIndex; index < this.tailingIndex; index++) {
            paged_data.push(this.EmployeeList[index]);
        }
        return paged_data;
    }

    btnNext_Click(){
        /**
         * Return Next List Data
         */
        let paged_data = [];
        if(this.tailingIndex == this.EmployeeList.length) return false;
        this.leadingIndex = this.leadingIndex + 1;
        this.tailingIndex = this.tailingIndex + 1;
        for (let index = this.leadingIndex; index < this.tailingIndex; index++) {
            paged_data.push(this.EmployeeList[index]);
        }
        return paged_data;
    }

    btnNextPage_Click(){
        /**
         * Return Next Page List Data
         */
        let paged_data = [];
        if(this.tailingIndex == this.EmployeeList.length) return false;
        
        this.leadingIndex = this.leadingIndex + 5;
        this.tailingIndex = this.tailingIndex + 5
        this.tailingIndex > this.EmployeeList.length ? this.tailingIndex = this.EmployeeList.length: '';
        this.tailingIndex>= this.EmployeeList.length ? this.leadingIndex = this.tailingIndex - 5:'';

        for (let index = this.leadingIndex; index < this.tailingIndex; index++) {
            paged_data.push(this.EmployeeList[index]);
        }
        return paged_data;
    }
    btnLast_Click(){
        /**
         * Return Last Set of Data
         */
        let paged_data = [];
        let leadIndex = 0;
        this.tailingIndex = this.EmployeeList.length;
        this.EmployeeList.length > 5 ? leadIndex = this.EmployeeList.length - 5 : '';
        this.leadingIndex = leadIndex;
        for (let index = leadIndex; index < this.EmployeeList.length; index++) {
            paged_data.push(this.EmployeeList[index])
            
        }
        return paged_data;
    }
}

//Instance Employee Class
const employee = new EmployeeData(EmployeeLIst);

/**
 *  Button and Table DOM Event Handler Below
 */

// On Load fetch data to table
$(document).ready(()=>{
$('#msg').hide();
 employee.purgeTable();
 employee.populateTable(employee.defaultTable_View());
 // Disable all handler button if data < = 5
 if(employee.EmployeeList.length <= 5 ){
     $('#btn_first').prop('disabled',true)
     $('#btn_last').prop('disabled',true)
     $('#btn_next').prop('disabled',true)
     $('#btn_prev').prop('disabled',true)
     $('#btn_prev_page').prop('disabled',true)
     $('#btn_next_page').prop('disabled',true)
 } 
 
})

// Button Handlers Below
$('#btn_first').click(()=>{
    const firstDataList = employee.btnFirst_Click();
    employee.purgeTable();
    employee.populateTable(firstDataList);
})


$('#btn_last').click(()=>{
    const lastDataList = employee.btnLast_Click();
    employee.purgeTable();
    employee.populateTable(lastDataList);
})


$('#btn_next').click(()=>{
    const nextDataList = employee.btnNext_Click();
    if(!nextDataList) {
       console.error('No Data Left to Preview');
       console.error('No Data Left to Preview');
       $('#msg').show("fast");
       $('#msg').append(`Already in Last Page`);  
   setTimeout(() => {
       $('#msg').hide("fast");
       $('#msg').empty();
   }, 1000);     
       return false;
    }
    employee.purgeTable();
    employee.populateTable(nextDataList);
    //console.log(nextDataList);
})

$('#btn_prev').click(()=>{
    const prevDataList = employee.btnPrevious_Click();
    if(!prevDataList) {
       console.error('No Data Left to Preview');
            $('#msg').show("fast");
            $('#msg').append(`Already in First Page`);  
        setTimeout(() => {
            $('#msg').hide("fast");
            $('#msg').empty();
        }, 1000);     
       return false;
    }
    employee.purgeTable();
    employee.populateTable(prevDataList);
})

$('#btn_prev_page').click(()=>{
    const prevPageDataList = employee.btnPreviousPage_Click();
    if(!prevPageDataList) {
       console.error('No Data Left to Preview');
       console.error('No Data Left to Preview');
       $('#msg').show("fast");
       $('#msg').append(`Already in First Page`);  
   setTimeout(() => {
       $('#msg').hide("fast");
       $('#msg').empty();
   }, 1000);     
       return false;
    }
    employee.purgeTable();
    employee.populateTable(prevPageDataList);
})

$('#btn_next_page').click(()=>{
    const nextPageDataList = employee.btnNextPage_Click();
    if(!nextPageDataList) {
       console.error('No Data Left to Preview');
       console.error('No Data Left to Preview');
       $('#msg').show("fast");
       $('#msg').append(`Already in Last Page`);  
   setTimeout(() => {
       $('#msg').hide("fast");
       $('#msg').empty();
   }, 1000);     
       return false;
    }
    employee.purgeTable();
    employee.populateTable(nextPageDataList);
})